# echo-golem

![echo golem logo](echo-golem-logo.png)

`echo-golem` is a project to demonstrate the ease of creating simple
[source-to-image](https://github.com/openshift/source-to-image) style
applications for cloud native platforms. Each example contained in this
repository implements an HTTP server that echoes all data sent to it at
the root(`/`) path.

The example applications are organized by language and framework
to indicate the foundational technologies in use.  Within each directory you
will find a `README.md` file containing complete instructions for deploying
and accessing the server.
